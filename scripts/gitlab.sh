#!/bin/bash

wget http://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm
yum install -y puppetlabs-release-pc1-el-7.noarch.rpm
rm -f puppetlabs-release-pc1-el-7.noarch.rpm
yum install -y puppet-agent
/opt/puppetlabs/bin/puppet module install vshn/gitlab

cat << EOF > /root/gitlab.pp
class { 'gitlab':
  external_url => 'http://gitlab.hacklab',
  edition      => 'ce',
}
EOF

/opt/puppetlabs/bin/puppet apply /root/gitlab.pp

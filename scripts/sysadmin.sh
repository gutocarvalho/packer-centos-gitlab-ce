#!/bin/bash

yum install wget
wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
yum install -y epel-release-latest-7.noarch.rpm
rm -f epel-release-latest-7.noarch.rpm
yum install -y net-tools git vim-enhanced wget rsync screen bind-utils nc telnet elinks lynx bzip2 unzip tcpdump man mlocate ccze htop traceroute
yum update -y

cat <<'EOF' > /root/.screenrc
shell -$SHELL
startup_message off
defscrollback 5000
hardstatus alwayslastline '%{= wk}%?%-Lw%?%{r}(%{k}%n*%f%t%?(%u)%?%{r})%{k}%?%+Lw%?%?%= %m/%d %C%A'
activity ''
termcapinfo xterm* 'hs:ts=\\E]2;:fs=\\007:ds=\\E]2;screen\\007'
EOF
